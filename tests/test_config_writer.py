import numpy as np
# -----------------------------------------------------------------------------
# Parameters (Modify the values below to suit your needs)
# -----------------------------------------------------------------------------

from dynamics import LWFA


###Parameters

##Gas parameters (Ar)

reldens = 0.5    # n_Ar(pateau1) * reldens = n_Ar(plateau2)
levelofion = 8   # estimate of ionisation levels of Argon (found by running
                 #simulations)

optimal_dens = True #When True n_Ar will not be further concidered
n_Ar = 1.4e25    #n_Ar(plateau2) (1/cm**3)

#Density ramp parameters
start = 0.e-6       #[m]
ramp1 = 1.e-4       #[m]
ramp2 = 1.e-4       #[m]
ramp3 = 1.e-4       #[m]
plateau1 = 0.8e-4   #[m]
plateau2 = 0.5e-3   #[m]

##Laser parameters ()

r_0 = 6e-4 #rms width [cm] a ~ exp(-(r/r_0)**2)
tau = 20e-15 #FWHM [s]
E = 350000 #energy of pulse [1e-7 J]
focus_position = start+ramp1+plateau1+ramp2+1e-5


config = {'reldens': reldens,
          'levelofion':levelofion,
          'optimal_dens':optimal_dens,
          'n_Ar':n_Ar,
          'start':start,
          'ramp1':ramp1,
          'ramp2':ramp2,
          'ramp3':ramp3,
          'plateau1':plateau1,
          'plateau2':plateau2,
          'r_0':r_0,
          'tau':tau,
          'E':E,
          'focus_position':focus_position}
import config_io

config_io.saveconfig(config)

print (config_io.readconfig()['r_0'])
