import matplotlib.pyplot as plt
import numpy as np

from opmd_viewer import OpenPMDTimeSeries


ts = OpenPMDTimeSeries( "diags/hdf5" )



zmin = -4.01e-5
zmax = 0.0e-5
ymax = 60

c  = 3e10

maxrho = 0
for it in ts.iterations:
    rho = ts.get_field(field = 'rho', iteration = it)[0]
    maxrhoi = np.max(np.abs(rho))
    if maxrhoi > maxrho:
	maxrho = maxrhoi


def plotBunch(i):
    fig = plt.figure(figsize=(7,4))
    it = ts.iterations[i]

    rho = ts.get_field(field = 'rho', iteration = it)[0]

    Er = ts.get_field(field = 'E', coord = 'r', iteration = it)[0]
    Et = ts.get_field(field = 'E', coord = 't', iteration = it)[0]
    Br = ts.get_field(field = 'B', coord = 'r', iteration = it)[0]
    Bt = ts.get_field(field = 'B', coord = 't', iteration = it)[0]

    ax1 = fig.add_axes([0.1,0.15,0.88,0.80])

    part = ts.get_particle(var_list = ['y','z','uy'],species = 'electron from Argon',iteration = it,select = {'uz' : [0.2,None]})
    yfast = part[0]
    zfast = part[1]
    uyfast = part[2]
   
    im_electrons = ax1.imshow(rho, origin = "lower",
			extent = [(zmin+c*1e-2*ts.t[i])*1e6,(zmax+c*1e-2*ts.t[i])*1e6,-ymax,ymax],aspect='auto', vmin = -maxrho, vmax = 	            maxrho,cmap=plt.cm.gray)
    from matplotlib.colors import LinearSegmentedColormap 
    cm_dict = {'red': ((0.0, 0.0, 0.0), (1.0, 0.0, 0.0)), 'green': ((0.0, 1.0, 1.0), (1.0, 1.0, 1.0)), 'blue': ((0.0, 0.0, 0.0), (1.0, 0.0, 0.0)), 'alpha': ((0.0, 0.0, 0.0),(1.0, 1.0, 1.0))} 
    my_cm = LinearSegmentedColormap('my_cm', cm_dict) 
    im_laser = ax1.imshow((Er*Bt - Et* Br)**2, origin = "lower",
			 extent = [(zmin+c*1e-2*ts.t[i])*1e6,(zmax+c*1e-2*ts.t[i])*1e6,-ymax,ymax],aspect='auto', cmap=my_cm)

    ax1.scatter(zfast,yfast,s = 0.1,color = 'red', edgecolor = None)


    plt.xlabel(r'z in $\mu m$')
    plt.ylabel(r'y in $\mu m$')
    plt.xlim([(zmin+c*1e-2*ts.t[i])*1e6,(zmax+c*1e-2*ts.t[i])*1e6])
    plt.ylim([ymax,-ymax])
    if len(yfast) != 0:
        print 'num of inj    : ',len(zfast)
        print 'max(abs(ux))  : ',max(abs(uyfast))
        print 'mean(abs(ux)) : ',np.mean(abs(uyfast))
        print 'sigma(ux)     : ',np.sqrt(np.var(uyfast))
        print 'beta_z gamma  : ',np.max(ts.get_particle(var_list = ['uz'],species = 'electron from Argon',iteration = it)[0]) 

    plt.savefig('video/topview/topview'+"%03d"%i)

from mpl_toolkits.mplot3d import Axes3D
def plotPhasespace(i):

    it = ts.iterations[i]

    speed = ts.get_particle(var_list = ['uz'],species = 'electron from Argon',iteration = it,select = {'uz' : [0.2,None]})[0]
    if len(speed) == 0:
        return

    maxspeed = np.max(speed)

    x, ux, y, uy, z, uz = ts.get_particle(var_list = ['x', 'ux', 'y', 'uy', 'z', 'uz'],species = 'electron from Argon',iteration = it,select = {'uz' : [maxspeed/4,None]})
    
    fig = plt.figure(figsize = (16*1.0,9*1.0))
    ax11 = fig.add_axes([0.05,0.05,0.17,0.40])
    ax11.set_title(r'x [$\mu m$] - $p_x$ [1]', x=0.3, y=0.9)
    ax11.ticklabel_format(style='sci', scilimits=(0,0))

    ax12 = fig.add_axes([0.05,0.50,0.17,0.40])
    ax12.set_title(r'y [$\mu m$] - $p_y$ [1]', x=0.3, y=0.9)
    ax12.ticklabel_format(style='sci', scilimits=(0,0))

    ax13 = fig.add_axes([0.25,0.50,0.17,0.40])
    ax13.set_title(r'z [$\mu m$] - $p_z$ [1]', x=0.3, y=0.9)
    ax13.ticklabel_format(style='sci', scilimits=(0,0))

    ax14 = fig.add_axes([0.25,0.05,0.17,0.40])
    ax14.set_title(r'x [$\mu m$] - y [$\mu m$]', x=0.3, y=0.9)
    ax14.ticklabel_format(style='sci', scilimits=(0,0))
    
    ax3d = fig.add_axes([0.45,0.1,0.5,0.89],projection='3d')
    
    dsize = 0.1
    ax11.scatter(x,ux,s = dsize, c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax11.set_xlim([-ymax,ymax])
    ax11.set_ylim([-1,1])
    ax12.scatter(y,uy,s = dsize,c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax12.set_xlim([-ymax,ymax])
    ax12.set_ylim([-1,1])
    ax13.scatter(z,uz,s = dsize, c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax13.set_xlim([(zmin+c*1e-2*0.9960439*ts.t[i])*1e6,(zmax+c*1e-2*0.9960439*ts.t[i])*1e6])
    ax13.set_ylim([0,30])
    ax14.scatter(x,y,s = dsize, c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax14.set_xlim([-ymax,ymax])
    ax14.set_ylim([-ymax,ymax])
    ax3d.scatter(z,x,y,s = dsize,c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax3d.set_ylim(-ymax,ymax)
    ax3d.set_zlim(-ymax,ymax)
    ax3d.set_xlim([(zmin+c*1e-2*ts.t[i])*1e6,(zmax+c*1e-2*ts.t[i])*1e6])
    plt.savefig('video/phasespace/phasespace'+"%03d"%i)
    plt.close("all")



    

def plotIonization(i):
    ny, nz = ts.get_field(field = 'rho', species = ts.avail_species[0])[0].shape
    ny = ny/10; nz = nz / 10
    it = ts.iterations[i]
    for l in range(2):
        plt.figure()
        hx = 10
        z, y, w = ts.get_particle(var_list = ['z', 'y', 'w'],species = 'Argon'+str(l)+'+',iteration = it,select = {'x' : [-hx/2,hx/2]})

        plt.title('Argon'+str(l)+'+')
        vol = (2*ymax*(zmax*1e6 - zmin*1e6)*hx)/ny/nz

        dens, zedges, yedges = np.histogram2d(z, y, bins = (nz, ny),weights = w,range=np.array([((zmin+c*1e-2*0.9960439*ts.t[i])*1e6,(zmax+c*1e-2*0.9960439*ts.t[i])*1e6), (-ymax, ymax)]))
        print np.max(dens)
        dens = dens.T/vol*1e6**3*1e-6

        plt.imshow(dens, extent=[zedges[0], zedges[-1], yedges[0], yedges[-1]],aspect='auto' )
        plt.colorbar()
#'Argon'+str(l)+'+'
print ts.avail_species
for i in [25]:
    #plotBunch(i)
    #plotPhasespace(i)
    plotIonization(i)
plt.show()

