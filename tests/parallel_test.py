# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 13:58:28 2017

@author: nicks
"""
import numpy as np
from pathos.multiprocessing import ProcessingPool as Pool
p = Pool(4)
class Test(object):
   def plus(self, x, y): 
     return x+y
 
t = Test()
x = np.arange(10)
y = np.arange(10)
print (p.map(t.plus, x, y))