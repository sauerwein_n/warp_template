# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 10:36:49 2017

@author: nicks
"""
import numpy as np

m = 9.109382e-28
e = 4.80320425e-10
c = 29979245800
P_0 = 0.5 #restmass electron [MeV]


def omega2n(omega):
    return omega**2*m/(4*np.pi*e**2)

def n2omega(n):
    return np.sqrt(4*np.pi*n*e**2/m)


class LWFA:
    def __init__(self, r_0, tau, E, n = None, lam = 800e-7):
        self.r_0 = r_0
        self.tau = tau
        self.E = E
        self.lam = lam
        
        ###########################################
        #These parameters are dicribed in section Simulation/Python of the Case study
        if n == None:
            self.set_optimal_n()
        else:
            self.n = n 
            self.omega = n2omega(n)
        

        self.omel = 2*np.pi*c/lam
        self.L = c*tau
        self.k = self.omega/c
        self.k_L = 2*np.pi/lam
        self.a_0 = np.sqrt(8/self.L)*e/(m*c**2*r_0*self.k_L)*np.sqrt(E)
        self.zend = self.L**3/lam**2
        self.Z_R = np.pi*r_0**2/lam
        self.epsilon = self.omega/self.omel

        self.beta_g =  (1-self.omega**2/(2*self.omel**2))
        self.gamma_g = 1/np.sqrt(1-self.beta_g**2)

        self.integrate_Phi()
        
        self.H_s = 1/self.gamma_g-np.min(self.Phi(self.xi))
        self.H_s_2d = 1/self.gamma_g
    
    def set_optimal_n(self):
        self.omega = 2.5/self.tau        
        self.n = omega2n(self.omega)  
        
    def integrate_Phi(self, xi = None):
        
        if xi == None:
            xi = np.linspace(-6*c*self.tau,1.2*c*self.tau,100)
            self.xi = xi
        
        def Phiint(xi):
            f = lambda x: self.k/2*np.sin(self.k*(xi-x))*self.a(x)**2

            from scipy.integrate import quad
            return -quad(f,xi,2*c*self.tau)[0]
        Phiint = np.vectorize(Phiint)
        from scipy.interpolate import interp1d
        self.Phi = interp1d(xi, Phiint(xi), kind='cubic',bounds_error = False)
        
        return self.Phi
        
    #def a(self, xi):
    #    return np.where(np.logical_and(xi<c*self.tau , xi>-c*self.tau), self.a_0*np.cos(np.pi/(2*c*self.tau)*xi),0)
    
    def a(self, xi):
        return self.a_0*np.exp(-2*np.log(2)*(xi/c/self.tau)**2)
        
    def uzp(self,xi,H_0):
        return P_0*(self.gamma_g**2*self.beta_g*(H_0 + self.Phi(xi)) + 
                    self.gamma_g*np.sqrt(self.gamma_g**2*(H_0+self.Phi(xi))**2-(1+self.a(xi)**2)))

    def uzm(self,xi,H_0):
        return P_0*(self.gamma_g**2*self.beta_g*(H_0 + self.Phi(xi)) - 
                    self.gamma_g*np.sqrt(self.gamma_g**2*(H_0+self.Phi(xi))**2-(1+self.a(xi)**2))) 
        
    def printvar(self):
        print ('\n \n Current parameters:')
        print ('omega        :',np.round(self.omega*1e-14,2),         ' \t 1e14 Hz     Plasma freq')
        print ('L            :',np.round(self.L*1e4,2),   ' \t mu m        Plasma wave length ')
        print ('a_0          :',np.round(self.a_0,2))
        print ('tau (FWHM)   :',np.round(self.tau*1e15,0),' \t fs          Laser pulse duration ')
        print ('L/ctau       :',np.round(self.L/(c*self.tau),2))
        print ('eta a_0 /2   :',np.round(self.eta(self.omega*self.tau)*self.a_0/2,2),' \t             Effiziency of plasma wave (sin profile)')
        print ('omega tau /pi:',np.round(self.omega*self.tau/np.pi,2))
        print ('n_e          :', np.round(self.n*1e-19,2), ' \t 1e19 1/cm^3 current electron density')
        print ('n_e          :', np.round(omega2n(self.omega)*1e-19,2), ' \t 1e19 1/cm^3 optimal electron density')
        print ('Max energy   :',np.round(P_0*np.max(self.uzp(self.xi,self.H_s)[~np.isnan(self.uzm(self.xi,self.H_s))]),2))
        print ('beta gamma   :',np.round(self.beta_g*self.gamma_g,2),      ' \t             Momentum of wave')
        print ('Z_R          :',np.round(self.Z_R*1e4,2),      '\t mu m        Rayleigh length   ')
    
    def eta(self, omegatau): 
        if omegatau < np.pi+1e-5 and omegatau > np.pi-1e-5:
            return np.pi/2
        return 1/(1-(omegatau/np.pi)**2)*np.sin(omegatau)

        