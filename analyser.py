## Analysis of Simulation
# 
# In order to understand the Physics of the injection process, and estimate the accelerated charge a simulation is performed.

import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

import os, sys, tarfile, wget
import os,glob
import numpy as np
import matplotlib.pyplot as plt
from scipy import constants
from opmd_viewer import OpenPMDTimeSeries

import matplotlib.animation as animation
import shutil

from NicePlots import format_axes
import config_io
from dynamics import LWFA, c, e, m
from pathos.multiprocessing import ProcessingPool as Pool

from mpl_toolkits.mplot3d import Axes3D

overview = False
video = True
extracted = False
spray = False

rmax =7e-6
n_cores = 6

minspeed = 0.5

write_output_to_file = False

if write_output_to_file:
	f = file('notebook_out.txt', 'w')
	sys.stdout = f



zmin = -6.e-5
zmax = 0.6e-5
ymax = 30
#load parameters from simulation script (warp_script.py)
config_file = 'configuration.txt'
config = config_io.readconfig(file = config_file)
    
reldens = config['reldens']
levelofion = config['levelofion']
optimal_dens = config['optimal_dens']
n_Ar = config['n_Ar']
start = config['start']
ramp1 = config['ramp1']
ramp2 = config['ramp2']
ramp3 = config['ramp3']
plateau1 = config['plateau1']
plateau2 = config['plateau2']
r_0 = config['r_0']
tau = config['tau']
E = config['E']
focus_position = config['focus_position']
 
#load data from simulation
ts = OpenPMDTimeSeries( "diags/hdf5" )

if optimal_dens:
    acc = LWFA(r_0, tau, E) #This class calculates all the important quanities
    n_Ar = acc.n / levelofion
else:
    acc = LWFA(r_0, tau, E, n = n_Ar * levelofion)

acc.printvar()





def dens_profile(z):
    if z < start or z > ramp1+plateau1+ramp2+plateau2+ramp3:
        return 0.
    if z < ramp1:
        return z/ramp1
    if z < ramp1+plateau1:
        return 1.
    if z < ramp1+plateau1+ramp2:
        return (reldens-1)/ramp2*(z-(ramp1+plateau1))+1
    if z < ramp1+plateau1+ramp2+plateau2:
        return reldens
    if z < ramp1+plateau1+ramp2+plateau2+ramp3:
        return reldens/ramp3*(ramp1+plateau1+ramp2+plateau2+ramp3-z)
    return 0.

def plot_dens_profile(dens_profile):

    zz = np.linspace(start,start+ramp1+ramp2+ramp3+plateau1+plateau2,1000)
    plt.figure(figsize = (5,3))
    plt.plot(zz*1e6,n_Ar/reldens*np.array([dens_profile(zzz) for zzz in zz]))
    plt.xlabel(r'z in $\mu m$')
    plt.ylabel(r'$n_{Ar}$ in cm$^{-3}$')
    #plt.ylim((0,3.e18))
    
    format_axes(plt.gca())
    plt.savefig('densityprofile.png')

    
plot_dens_profile(dens_profile)
# ## Analysis of the injection process
# First we want to see how many particles are injected and analyse how much they are accelerated.

def get_maxu(it):
    bg=ts.get_particle(var_list = ['uz'],species = 'electron from Argon',select = {'uz' : [minspeed,None]}, iteration = it)[0]
    if len(bg) == 0:
        return None
    else:
        return np.max(bg)

def get_charge(it):
    e = 1.6e-19
    w = ts.get_particle(var_list = ['w'],species = 'electron from Argon',iteration = it, select = {'uz' : [minspeed,None]})[0]
    if len(w) == 0:
        return 0
    return np.sum(w) * e
                                  
# In this part a nice way to visualize the particles will be presented. The electrons of the wake will be ploted using a histogramm in 2D. The injected particles however are represented as dots in the plt. Like this we can see how they propagate.




z_extent = lambda i: ((zmin+c*acc.beta_g*1e-2*ts.t[i])*1e6,(zmax+c*acc.beta_g*1e-2*ts.t[i])*1e6)


def plotBunch(i):
    
    zl, zu = z_extent(i) 

    fig = plt.figure(figsize=(7,4))
    it = ts.iterations[i]

    rho = ts.get_field(field = 'rho', iteration = it)[0]

    Er = ts.get_field(field = 'E', coord = 'r', iteration = it)[0]
    Et = ts.get_field(field = 'E', coord = 't', iteration = it)[0]
    Br = ts.get_field(field = 'B', coord = 'r', iteration = it)[0]
    Bt = ts.get_field(field = 'B', coord = 't', iteration = it)[0]

    ax1 = fig.add_axes([0.1,0.15,0.88,0.80])

    speed = ts.get_particle(var_list = ['uz'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]

    if len(speed) != 0:
        maxspeed = np.max(speed)
    	yfast, zfast, uyfast = ts.get_particle(var_list = ['y','z','uy'],species = 'electron from Argon',iteration = it,select = {'uz' : [maxspeed/4,None]})
    else:
	yfast, zfast, uyfast = [[],[],[]]
   
    im_electrons = ax1.imshow(rho, origin = "lower",
			extent = [zl, zu,-ymax,ymax],aspect='auto', vmin = -maxrho, vmax = maxrho,cmap=plt.cm.gray)
    from matplotlib.colors import LinearSegmentedColormap 
    cm_dict = {'red': ((0.0, 0.0, 0.0), (1.0, 0.0, 0.0)), 'green': ((0.0, 1.0, 1.0), (1.0, 1.0, 1.0)), 'blue': ((0.0, 0.0, 0.0), (1.0, 0.0, 0.0)), 'alpha': ((0.0, 0.0, 0.0),(1.0, 1.0, 1.0))} 
    my_cm = LinearSegmentedColormap('my_cm', cm_dict) 
    im_laser = ax1.imshow((Er*Bt - Et* Br)**2, origin = "lower",
			 extent = [zl, zu,-ymax,ymax],aspect='auto', cmap=my_cm)

    #ax1.scatter(zfast,yfast,s = 0.1,color = 'red', edgecolor = None)


    plt.xlabel(r'z in $\mu m$')
    plt.ylabel(r'y in $\mu m$')
    plt.xlim(z_extent(i))
    plt.ylim([ymax,-ymax])
    if len(yfast) != 0:
        print 'num of inj    : ',len(zfast)
        print 'max(abs(ux))  : ',max(abs(uyfast))
        print 'mean(abs(ux)) : ',np.mean(abs(uyfast))
        print 'sigma(ux)     : ',np.sqrt(np.var(uyfast))
        print 'beta_z gamma  : ',np.max(ts.get_particle(var_list = ['uz'],species = 'electron from Argon',iteration = it)[0]) 

    plt.savefig('video/topview/topview'+"%03d"%i)
    plt.close("all")



def plotPhasespace(i):

    it = ts.iterations[i]

    speed = ts.get_particle(var_list = ['uz'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]
    if len(speed) == 0:
        return

    maxspeed = np.max(speed)

    x, ux, y, uy, z, uz = ts.get_particle(var_list = ['x', 'ux', 'y', 'uy', 'z', 'uz'],species = 'electron from Argon',iteration = it,select = {'uz' : [maxspeed/4,None]})
    
    fig = plt.figure(figsize = (16*1.0,9*1.0))
    ax11 = fig.add_axes([0.05,0.05,0.17,0.40])
    ax11.set_title(r'x [$\mu m$] - $p_x$ [1]', x=0.3, y=0.9)
    ax11.ticklabel_format(style='sci', scilimits=(0,0))

    ax12 = fig.add_axes([0.05,0.50,0.17,0.40])
    ax12.set_title(r'y [$\mu m$] - $p_y$ [1]', x=0.3, y=0.9)
    ax12.ticklabel_format(style='sci', scilimits=(0,0))

    ax13 = fig.add_axes([0.25,0.50,0.17,0.40])
    ax13.set_title(r'z [$\mu m$] - $p_z$ [1]', x=0.3, y=0.9)
    ax13.ticklabel_format(style='sci', scilimits=(0,0))

    ax14 = fig.add_axes([0.25,0.05,0.17,0.40])
    ax14.set_title(r'x [$\mu m$] - y [$\mu m$]', x=0.3, y=0.9)
    ax14.ticklabel_format(style='sci', scilimits=(0,0))
    
    ax3d = fig.add_axes([0.45,0.1,0.5,0.89],projection='3d')
    


    dsize = 0.1
    ax11.scatter(x,ux,s = dsize, c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax11.set_xlim([-ymax,ymax])
    ax11.set_ylim([-1,1])
    ax12.scatter(y,uy,s = dsize,c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax12.set_xlim([-ymax,ymax])
    ax12.set_ylim([-1,1])
    ax13.scatter(z,uz,s = dsize, c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax13.set_xlim(z_extent(i))
    ax13.set_ylim([0,30])
    ax14.scatter(x,y,s = dsize, c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax14.set_xlim([-ymax,ymax])
    ax14.set_ylim([-ymax,ymax])
    ax3d.scatter(z,x,y,s = dsize,c = np.sqrt((ux**2 + uy**2 + uz**2)), edgecolor = 'None')
    ax3d.set_ylim(-ymax,ymax)
    ax3d.set_zlim(-ymax,ymax)
    ax3d.set_xlim(z_extent(i))
    plt.savefig('video/phasespace/phasespace'+"%03d"%i)
    plt.close("all")


maxrho = 0

if video:
    print 'find maximal rho'
    
    for it in ts.iterations:
        rho = ts.get_field(field = 'rho', iteration = it)[0]
        maxrhoi = np.max(np.abs(rho))
        if maxrhoi > maxrho:
	    maxrho = maxrhoi

# Here we will make a video of the traveling laser pulse with the injected particles.
print 'Initialize ,'+str(n_cores)+' workers'
p = Pool(n_cores)
print 'sucessful!'
if overview:
    #initalize pool for parallel computing

    print '-------------------------------------'
    print 'Overview'
    print ''
    print 'Plot maximal Energy ...'
    plt.figure(figsize = (3,3))

    betagamma = p.map(get_maxu, ts.iterations[:-1])
    plt.plot(ts.t[:-1]*1e4*c,np.array(betagamma, dtype = float)*0.5)
    plt.ylabel(r'$p_z$ in MeV/c')
    plt.xlabel(r'z in $\mu m$')
    format_axes(plt.gca())
    plt.savefig('maxuz_reld_'+str(reldens)+'.png')
    print 'sucessful!'


    print 'Plot injected Charge ...'
    plt.figure(figsize = (3,3))
    charge = p.map(get_charge,ts.iterations[:-1])
    print charge
    plt.plot(ts.t[:-1]*1e4*c,np.array(charge)*1e12)
    plt.xlabel(r'z in $\mu m$')
    plt.ylabel('injected Charge in pC')
    format_axes(plt.gca())
    plt.savefig('numbunch_reld_'+str(reldens)+'.png')
    print 'sucessful!'



if video:

    #initalize pool for parallel computing
    
    print '-------------------------------------'
    print 'Video'
    print ''
    print 'Topview ...'

    p.map(plotBunch, np.arange(len(ts.iterations)))

    print 'Sucessful!'

    print ''
    print 'Phasespace ...'

    #p.map(plotPhasespace, np.arange(len(ts.iterations)))


    print 'Sucessful!'



# ## Extracted beam
# 
# At this point we look at the phase space of the extracted beam. What is the accelerated charge, mean momentum, radius. Then we track the particles using linear paraxial approximation (drift matix, quadrupole 1. order, ...). The code used was writen for the course PAM 1 at ETH.



if extracted:
	print '-------------------------------------'
	print 'Extracted'
	print ''
	x = ts.get_particle(var_list = ['x'],species = 'electron from Argon',iteration = ts.iterations[-1],select = {'uz' : [uzmin,None]})[0]
	ux = ts.get_particle(var_list = ['ux'],species = 'electron from Argon',iteration = ts.iterations[-1],select = {'uz' : [uzmin,None]})[0]
	y = ts.get_particle(var_list = ['y'],species = 'electron from Argon',iteration = ts.iterations[-1],select = {'uz' : [uzmin,None]})[0]
	uy = ts.get_particle(var_list = ['uy'],species = 'electron from Argon',iteration = ts.iterations[-1],select = {'uz' : [uzmin,None]})[0]
	z = ts.get_particle(var_list = ['z'],species = 'electron from Argon',iteration = ts.iterations[-1],select = {'uz' : [uzmin,None]})[0]
	uz = ts.get_particle(var_list = ['uz'],species = 'electron from Argon',iteration = ts.iterations[-1],select = {'uz' : [uzmin,None]})[0]
	w = ts.get_particle(var_list = ['w'],species = 'electron from Argon',iteration = ts.iterations[-1],select = {'uz' : [uzmin,None]})[0]
	
	np.savetxt('bunch.txt', (x,ux,y,uy,z,uz,w), delimiter = '\t',header = str(len(x))) 
	
	

	
	charge = np.sum(w) * 1.6e-19 * 1e12
	print 'Charge in pC: ',charge
	
	

	
	from MainAcceLegoRator import *
	beam = np.transpose([x/1e6,ux,y/1e6,uy,z/1e6,uz])
	simlength = 0.1
	
	print 'Mean momentum in MeV:', np.average(uz,weights = w)*0.5
	
	
	accelerator = [['d',[0.1]]]
	setuprun(accelerator, simlength, beam, weights = w)
	plt.savefig('finalbunch.png')
	print 'Sucessful!'
	
# ## Radiation savety
# For the final experiment it is very crutial to know how much charge is accelerated. Here we do also concider particles, that leave the simulation window during the run.





def get_spray():    

    def get_particles_zylinder(i):
        particles = [[1,1,1,1,1,1,0]]
        it = ts.iterations[i]
        dt = ts.t[i+1]-ts.t[i]
        x = ts.get_particle(var_list = ['x'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]
        if len(x) == 0:
            return particles
        ux = ts.get_particle(var_list = ['ux'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]
        y = ts.get_particle(var_list = ['y'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]
        uy = ts.get_particle(var_list = ['uy'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]
        z = ts.get_particle(var_list = ['z'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]
        uz = ts.get_particle(var_list = ['uz'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]
        w = ts.get_particle(var_list = ['w'],species = 'electron from Argon',iteration = it,select = {'uz' : [minspeed,None]})[0]
        for j in range(len(x)):
            gamma = np.sqrt(1+ux[j]**2+uy[j]**2+uz[j]**2)
            r = np.sqrt(x[j]**2+y[j]**2)
            if r*1e-6 > rmax-dt*c*(ux[j]*x[j]+uy[j]*y[j])/(gamma*r) and r*1e-6 < rmax:# or z[j]*1e-6 < zmin+ts.t[i+1]*c*0.9960439-dt*c*uz[j]/gamma:
                particles += [[x[j],ux[j],y[j],uy[j],z[j],uz[j],w[j]]]
        return particles


    
    res = p.map(get_particles_zylinder, range(len(ts.iterations)-1))

    particles = np.vstack(res)    

    np.save("particles"+str(rmax),particles)
    return particles



if spray:
    print '-------------------------------------'
    print 'Spray'
    print ''
    import os.path
    if os.path.isfile("particles"+str(rmax)+".npy") == False:
	    particles = get_spray()
    else:
	    particles = np.load("particles"+str(rmax)+".npy")



    particles = np.array(particles)
    x = particles.T[0]
    ux = particles.T[1]
    y = particles.T[2]
    uy = particles.T[3]
    z = particles.T[4]
    uz = particles.T[5]
    w = particles.T[6]
    plt.figure(figsize = (3,3))
    #plt.scatter(z,x,s = 1,alpha = 1,c = 'red')
    plt.scatter(x,y,s = 1,alpha = 1,c = 'red')
    print 'Charge in pC          ',sum(w)*1.6e-19*1e12
    print 'Mean beta gamma        ',np.mean(np.sqrt(ux**2+uz**2+uy**2+1))
    print 'Mean radial beta gamma ',np.mean(np.sqrt(uy**2+ux**2))
    print 'Mean logitu beta gamma ',np.mean(uz)



    N = 31
    plt.figure()
    theta = np.linspace(-np.pi,np.pi, N)
    ax = plt.subplot(111, polar=True)
    phi = np.arctan2(y, x)
    width = (2*np.pi) / N
    radii = np.histogram(phi,bins = theta, weights = w*1.6e-19*1e12)[0]
    bars = ax.bar(theta[:-1], radii/(2*np.pi)*N, width=width)
    ax.set_rmin(-0.5)
    plt.title('Azimuthal Distribution of Charge in pC/rad',x = 0.1,y = 	1.05)
    plt.gcf().tight_layout()
    plt.savefig('angularspray')

    plt.figure()
    plt.hist(np.sqrt(uz**2+uy**2+ux**2)*0.5,bins = 30, weights = 	w*1.6e-19*1e12)
    plt.xlabel('Momentum in Mev')
    plt.ylabel('Charge in pC')
    plt.gcf().tight_layout()
    plt.savefig('energyspray')


    ur = np.sqrt(ux**2 + uy**2)
    plt.figure()
    theta = np.linspace(0,np.pi/4, N)
    ax = plt.subplot(111, polar=True)
    phi = np.arctan2(uz, ur)
    width = (np.pi/2) / N
    radii = np.histogram(phi,bins = theta, weights = w*1.6e-19*1e12)[0]
    bars = ax.bar(theta[:-1], radii/np.pi*N*8, width=width)
    bars = ax.bar(-theta[:-1], radii/np.pi*N*8, width=width)
    ax.set_rmin(-0.5)
    plt.title('Altitudal Distribution of Charge in pC/rad',x = 0.1,y = 	1.05)
    plt.gcf().tight_layout()
    plt.savefig('direction')    

    theta_acc = np.pi/5
    charge_acc = np.sum(np.where(np.logical_or(phi < theta_acc, phi > 2*np.pi - theta_acc), w, np.zeros_like(w)))*1.6e-19*1e12
    print 'Charge in '+str(theta_acc)+': ',charge_acc,' pC'

    print 'Sucessful!'
	



