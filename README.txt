README

In order to get the results presented in the Semesterthesis by NICK SAUERWEIN follow the next steps.

1) Import the right gcc module and python:
	module load gcc/gcc-4.7.2
	alias qq='qstat -f -u "*" '
	alias qbd='qstat -f -q prime_bd.q -u "*"'
	export PATH=$HOME/miniconda2/bin/:$PATH

2) Set the simulation parameters a desired:
- open warp_script.py with editor and set the parameters
- lower the parameters N_steps and Nz by the same factor to get less resoluting
CAUTION: the results are then not physical anymore (numerical dispersion)

3) Run the simulation:
- set number of cores in run.sge:
	line 4: #$ -pe orte 128  (for 128 cores)
	line 15: ARGS=' warp_script.py -p 4 4 8' (4 * 4 * 8 = 128 spatial decomposition)
- commit job to merlin cluster:
	qsub run.sge
- look at progress using:
	tail -f LPWA-1.o*

4) Create plots and data analyis:
- open notebook.py and set which plots to generate:
	line 13: overview = True
	line 14: video = True
	line 15: extracted = True
	line 16: spray = True
- look at progess using:
	tail -f notebook_out.txt

If problems occur, please contact me under nicksauerwein@hotmail.de
