import numpy as np

c = 299792458

#accelerator parts
def rotation(phi):
    return np.array([[np.cos(phi),0,np.sin(phi),0,0,0],
                     [0,np.cos(phi),0,np.sin(phi),0,0],
                     [-np.sin(phi),0,np.cos(phi),0,0,0],
                     [0,-np.sin(phi),0,np.cos(phi),0,0],
                     [0,0,0,0,1,0],
                     [0,0,0,0,0,1]])

def driftSpace(para,setup):
    L = para[0]
    beta_0 = setup[1]
    gamma_0 = setup[2]
    #print L
    dri = np.array([[1,L,0,0,0,0],
                                  [0,1,0,0,0,0],
                                  [0,0,1,L,0,0],
                                  [0,0,0,1,0,0],
                                  [0,0,0,0,1,L/(beta_0*gamma_0)**2],
                                  [0,0,0,0,0,1]])
    #print ("dri",dri)
    return dri.T
        
def diPole(para,setup):
    P_0 = setup[0]
    q = setup[4]
    m = setup[3]
    beta_0 = setup[1]
    gamma_0 = setup[2]
    w = q/P_0*para[1]
    L = para[0]
    r = rotation(para[2])
    r_i = rotation(-para[2])
    K1 = -q/P_0*para[1]*np.tan(para[3]*L*q*para[1]/(2*np.pi*P_0))
    fringe = np.array([[1,0,0,0,0,0],
                       [-K1,1,0,0,0,0],
                       [0,0,1,0,0,0],
                       [0,0,K1,1,0,0],
                       [0,0,0,0,1,0],
                       [0,0,0,0,0,1]])
    di = np.array([[np.cos(w*L),np.sin(w*L)/w,0,0,0,(1-np.cos(w*L))/(w*beta_0)],
                   [-w*np.sin(w*L),np.cos(w*L),0,0,0,np.sin(w*L)/beta_0],
                   [0,0,1,L,0,0],
                   [0,0,0,1,0,0],
                   [-np.sin(w*L)/beta_0,-(1-np.cos(w*L))/(w*beta_0),0,0,1,L/(beta_0*gamma_0)**2-(w*L-np.sin(w*L))/(w*beta_0**2)],
                   [0,0,0,0,0,1]])
    #print ("di",di)
    return (np.dot(np.dot(np.dot(np.dot(r , fringe) , di) , fringe) , r_i)).T


def quadroPole(para,setup):
    beta_0 = para[1]
    P_0 = setup[0]
    q = setup[4]
    beta_0 = setup[1]
    gamma_0 = setup[2]
    w = np.sqrt(q/P_0*para[1])
    #print ('qp',w)
    L = para[0]
    r = rotation(para[2])
    r_i = rotation(-para[2])
    #print L
    quad = np.array([[np.cos(w*L),np.sin(w*L)/w,0,0,0,0],
                                                  [-w*np.sin(w*L),np.cos(w*L),0,0,0,0],
                                                  [0,0,np.cosh(w*L),np.sinh(w*L)/w,0,0],
                                                  [0,0,w*np.sinh(w*L),np.cosh(w*L),0,0],
                                                  [0,0,0,0,1,L/(beta_0*gamma_0)**2],
                                                  [0,0,0,0,0,1]])
    #print ("quad",r)
    return np.dot(np.dot(r , quad) , r_i).T	
