"""
This is a typical input script that runs a simulation of
laser-wakefield acceleration using Warp 2D / Circ.

Usage
-----
- Modify the parameters below to suit your needs
- Type "python -i warp_script.py" in a terminal
- When the simulation finishes, the python session will *not* quit.
    Therefore the simulation can be continued by running step()
    Otherwise, one can just type exit()
"""
# Import warp-specific packages
from warp.init_tools import *
import numpy as np
import config_io
# -----------------------------------------------------------------------------
# Parameters (Modify the values below to suit your needs)
# -----------------------------------------------------------------------------

from dynamics import LWFA


#if you want to run some special configuration, put config file name here
config_file = None

if config_file == None:
    ###Parameters

    ##Gas parameters (Ar)

    reldens = 0.5    # n_Ar(pateau1) * reldens = n_Ar(plateau2)
    levelofion = 8   # estimate of ionisation levels of Argon (found by running
                 #simulations)

    optimal_dens = True #When True n_Ar will not be further concidered
    n_Ar = 4e17    #n_Ar(plateau2) (1/cm**3)

    #Density ramp parameters
    start = 0.e-6       #[m]
    ramp1 = 1.e-4       #[m]
    ramp2 = 1.e-4       #[m]
    ramp3 = 0.1e-4       #[m]
    plateau1 = 0.8e-4   #[m]
    plateau2 = 0.25e-3   #[m]

    ##Laser parameters ()

    r_0 = 6e-4 #rms width [cm] a ~ exp(-(r/r_0)**2)
    tau = 20e-15 #FWHM [s]
    E = 350000 #energy of pulse [1e-7 J]
    focus_position = start+ramp1+plateau1+ramp2+1e-5


    config = {'reldens': reldens,
              'levelofion':levelofion,
              'optimal_dens':optimal_dens,
              'n_Ar':n_Ar,
              'start':start,
              'ramp1':ramp1,
              'ramp2':ramp2,
              'ramp3':ramp3,
              'plateau1':plateau1,
              'plateau2':plateau2,
              'r_0':r_0,
              'tau':tau,
              'E':E,
              'focus_position':focus_position}

    config_io.saveconfig(config)

else:
    config = config_io.readconfig(file = config_file)
    
    reldens = config['reldens']
    levelofion = config['levelofion']
    optimal_dens = config['optimal_dens']
    n_Ar = config['n_Ar']
    start = config['start']
    ramp1 = config['ramp1']
    ramp2 = config['ramp2']
    ramp3 = config['ramp3']
    plateau1 = config['plateau1']
    plateau2 = config['plateau2']
    r_0 = config['r_0']
    tau = config['tau']
    E = config['E']
    focus_position = config['focus_position']
    
    
    
scale_sim = 1 #Make simulation faster, but not physical. 
              #Numerical Dispersion!! (set 1 to get physical results)

##########
#If you just want run simulation related to the PSI LWFA experiment, you don't
#need to change the later code.



if optimal_dens:
    acc = LWFA(r_0, tau, E) #This class calculates all the important quanities
    n_Ar = acc.n / levelofion
else:
    acc = LWFA(r_0, tau, E, n = n_Ar * levelofion)

acc.printvar()



# General parameters
# ------------------
# Dimension of simulation ("3d", "circ", "2d", "1d")
dim = "2d"
# Number of azimuthal modes beyond m=0, for "circ" (not used for "2d" and "3d")
circ_m = 2
# Total number of timesteps in the simulation
N_steps = 1.2*24000/scale_sim
# Whether to run the simulation interactively (0:off, 1:on)
interactive = 0




# Simulation box
# --------------
# Number of grid cells in the longitudinal direction
Nz = 1.2*1650/scale_sim
# Number of grid cells in transverse direction (represents Nr in "circ")
Nx = 400/scale_sim
# Number of grid cells in the 3rd dimension (not used for "2d" and "circ")
Ny = 50
# Dimension of the box in longitudinal direction (meters)
zmin = -6.e-5
zmax = 0.6e-5
# Dimension of the box in transverse direction (box ranges from -xmax to xmax)
xmax = 30.e-6
# Dimension of the box in 3rd direction (not used for "2d" and "circ")
ymax = 15.e-6

# Field boundary conditions (longitudinal and transverse respectively)
f_boundz  = openbc
f_boundxy = openbc
if dim == "circ":
    f_boundxy = dirichlet
# Particles boundary conditions (longitudinal and transverse respectively)
p_boundz  = absorb
p_boundxy = absorb

# Moving window (0:off, 1:on)
use_moving_window = 1
# Speed of the moving window (ignored if use_moving_window = 0)
v_moving_window = clight * acc.beta_g

# Diagnostics
# -----------
# Period of diagnostics (in number of timesteps)
diag_period = 300/scale_sim
# Whether to write the fields
write_fields = 1
# Whether to write the particles
write_particles = 1
# Whether to write the diagnostics in parallel
parallel_output = False

# Numerical parameters
# --------------------
# Field solver (0:Yee, 1:Karkkainen on EF,B, 3:Lehe)
stencil = 0
# Particle shape (1:linear, 2:quadratic, 3:cubic)
depos_order = 2
# Gathering mode (1:from cell centers, 4:from Yee mesh)
efetch = 1
# Particle pusher (0:Boris, 1:Vay)
particle_pusher = 1

# Current smoothing parameters
# ----------------------------
# Turn current smoothing on or off (0:off; 1:on)
use_smooth = 1 
# Number of passes of smoother and compensator in each direction (x, y, z)
npass_smooth = array([[ 0 , 0 ], [ 0 , 0 ], [ 1 , 1 ]])
# Smoothing coefficients in each direction (x, y, z)
alpha_smooth = array([[ 0.5, 3.], [ 0.5, 3.], [0.5, 3./2]])
# Stride in each direction (x, y, z)
stride_smooth = array([[ 1 , 1 ], [ 1 , 1 ], [ 1 , 1 ]])

# Laser parameters
# ----------------
# Initialize laser (0:off, 1:on)
use_laser = 1
# Position of the antenna (meters)
laser_source_z = -0.5e-5
# Polarization angle with respect to the x axis (rad)
laser_polangle = pi/2

# Laser file:
# When using a laser profile that was experimentally
# measured, provide a string with the path to an HDF5 laser file,
# otherwise provide None and a Gaussian pulse will be initialized
laser_file = None
laser_file_energy = 2. # When using a laser file, energy in Joule of the pulse

# Gaussian pulse:
# Laser amplitude at focus

laser_a0 = acc.a_0
# Waist at focus (meters)
laser_w0 = acc.r_0/100
# Length of the pulse (length from the peak to 1/e of the amplitude ; meters)
laser_ctau = clight * acc.tau/(2*np.sqrt(np.log(2))) #acc.tau = FWHM (see definition in Masterthesis)
# Initial position of the centroid (meters)
laser_z0 = -2 * laser_ctau
# Focal position
laser_zfoc = focus_position

# Plasma macroparticles
# ---------------------
# Initialize some preexisting plasmas electrons (0:off, 1:on)
# (Can be used in order to neutralize pre-ionized ions, if any,
# or in order to simulate a plasma without having to initialize ions)
use_preexisting_electrons = 0
# Initialize plasma ions (0:off, 1:on)
use_ions = 1
# Number of macroparticles per cell in each direction
# In Circ, nppcelly is the number of particles along the
# azimuthal direction. Use a multiple of 4*circ_m
plasma_nx = 2
plasma_ny = 8
plasma_nz = 2


# Plasma content and profile
# --------------------------
# Reference plasma density (in number of particles per m^3)
n_plasma = n_Ar/reldens*1e6
# Relative density of the preexisting electrons (relative to n_plasma)
rel_dens_preexisting_electrons = 0
# The different elements used. (Only used if use_ions is different than 0.)
# relative_density is the density relative to n_plasma.
# q_start is the ionization state of the ions at the beginning of the simulation
# q_max is the maximum ionization state
# If q_start is not equal to q_max, ionization between states will be computed.
ion_states = {  'Argon': {'relative_density':1., 'q_start':0, 'q_max':11 } }
# Positions between which the plasma is initialized
# (Transversally, the plasma is initialized between -plasma_xmax and
# plasma_xmax, along x, and -plasma_ymax and plasma_ymax along y)
plasma_zmin = 1.e-6
plasma_zmax = 1500.e-6
plasma_xmax = xmax
plasma_ymax = ymax

# Define your own profile and profile parameters below
def dens_profile(z):
    if z < start or z > ramp1+plateau1+ramp2+plateau2+ramp3:
        return 0.
    if z < ramp1:
        return z/ramp1
    if z < ramp1+plateau1:
        return 1.
    if z < ramp1+plateau1+ramp2:
        return (reldens-1)/ramp2*(z-(ramp1+plateau1))+1
    if z < ramp1+plateau1+ramp2+plateau2:
        return reldens
    if z < ramp1+plateau1+ramp2+plateau2+ramp3:
        return reldens/ramp3*(ramp1+plateau1+ramp2+plateau2+ramp3-z)
    return 0.

def plasma_dens_func( x, y, z ):
    """
    User-defined function: density profile of the plasma
    
    It should return the relative density with respect to n_plasma,
    at the position x, y, z (i.e. return a number between 0 and 1)

    Parameters
    ----------
    x, y, z: 1darrays of floats
        Arrays with one element per macroparticle
    Returns
    -------
    n : 1d array of floats
        Array of relative density, with one element per macroparticles
    """
    return (np.array([dens_profile(zz) for zz in z]))

# Relativistic beam
# -----------------
# Initialize beam electrons (0:off, 1:on)
# (Please be aware that initializing a beam in 2D geometry makes very little
# physical sense, because of the long range of its space-charge fields) 
use_beam = 0
# Longitudinal momentum of the beam
beam_uz = 100.
# Beam density
n_beam = 1.e26
# Number of macroparticles per cell in each direction
beam_nx = 2*plasma_nx
beam_ny = 2*plasma_ny
beam_nz = 2*plasma_nz
# Positions between which the beam is initialized
# (Transversally, the plasma is initialized between -plasma_xmax and
# plasma_xmax, along x, and -plasma_ymax and plasma_ymax along y)
beam_zmin = -12.e-6
beam_zmax = -10.e-6
beam_xmax = 3.e-6
beam_ymax = 3.e-6

# Define your own profile and profile parameters below
beam_rmax = beam_xmax
def beam_dens_func(x, y, z):
    """
    User-defined function: density profile of the beam
    
    It should return the relative density with respect to n_beam,
    at the position x, y, z (i.e. return a number between 0 and 1)

    Parameters
    ----------
    x, y, z: 1darrays of floats
        Arrays with one element per macroparticle
    Returns
    -------
    n : 1d array of floats
        Array of relative density, with one element per macroparticles
    """
    # Allocate relative density
    n = ones_like(z)
    # Longitudinal profile: parabolic
    n = n*(z - beam_zmin)*(beam_zmax - z) * 4/(beam_zmax - beam_zmin)**2
    # Transverse profile: parabolic
    r = sqrt( x**2 + y**2)
    n = n*(1 - (r/beam_rmax)**2 )
    # Put the density above rmax to 0
    n[r > beam_rmax] = 0.

    return(n)

# -----------------------------------------------------------------------------
# Initialization of the simulation (Normal users should not modify this part.)
# -----------------------------------------------------------------------------

# Set some general options for warp
set_diagnostics( interactive )
set_boundary_conditions( f_boundz, f_boundxy, p_boundz, p_boundxy )
set_simulation_box( Nz, Nx, Ny, zmin, zmax, xmax, ymax, dim )
set_moving_window( use_moving_window, v_moving_window )

# See smoothing.py
set_smoothing_parameters( use_smooth, dim, npass_smooth,
                         alpha_smooth, stride_smooth )

# Creation of the species
# -----------------------

elec = None
ions = None
elec_from_ions = None
beam = None
# Create the plasma species
# Reference weight for plasma species
plasma_weight = prepare_weights( n_plasma, plasma_nx, plasma_ny,
                            plasma_nz, dim, circ_m )
if use_preexisting_electrons:
    elec_weight = rel_dens_preexisting_electrons * plasma_weight
    elec = Species(type=Electron, weight=elec_weight, name='electrons')
if use_ions:
    ions, elec_from_ions = initialize_ion_dict( ion_states, plasma_weight,
                                                group_elec_by_element=True )
# Create the beam
if use_beam:
    beam_weight = prepare_weights( n_beam, beam_nx, beam_ny,
                                   beam_nz, dim, circ_m )
    beam = Species(type=Electron, weight=beam_weight, name='beam')
# Set the numerical parameters only now: they affect the newly created species
set_numerics( depos_order, efetch, particle_pusher, dim)

# Setup the field solver object
# -----------------------------
em = EM3D(
    stencil=stencil,
    npass_smooth=npass_smooth,
    alpha_smooth=alpha_smooth,
    stride_smooth=stride_smooth,
    l_2dxz= (dim in ["2d", "circ"]),
    l_2drz= (dim in ["circ"]),
    l_1dz = (dim =="1d" ),
    l_getrho=True,
    circ_m = (dim =="circ")*circ_m,
    type_rz_depose=1 )
registersolver(em)

# Introduce the laser
# -------------------
if use_laser==1:
    add_laser( em, dim, laser_a0, laser_w0, laser_ctau, laser_z0,
        zf=laser_zfoc, theta_pol=laser_polangle, source_z=laser_source_z,
        laser_file=laser_file, laser_file_energy=laser_file_energy )

# Introduce the beam
# ------------------
# Load the beam
if use_beam:
    PlasmaInjector( beam, None, w3d, top, dim, beam_nx, beam_ny, beam_nz,
                beam_zmin, beam_zmax, beam_xmax, beam_ymax,
                dens_func = beam_dens_func, uz_m=beam_uz )
    initialize_beam_fields( em, dim, beam, w3d, top )

# Introduce the plasma
# --------------------
# Create an object to store the information about plasma injection
plasma_injector = PlasmaInjector( elec, ions, w3d, top, dim,
        plasma_nx, plasma_ny, plasma_nz, plasma_zmin,
        plasma_zmax, plasma_xmax, plasma_ymax, plasma_dens_func )
# Continuously inject the plasma, if the moving window is on
if use_moving_window :
    installuserinjection( plasma_injector.continuous_injection )
        
# Setup the diagnostics
# ---------------------
if write_fields == 1:
    diag1 = FieldDiagnostic( period=diag_period, top=top, w3d=w3d, em=em,
                comm_world=comm_world, lparallel_output=parallel_output )
    installafterstep( diag1.write )
if write_particles == 1:
    diag2 = ParticleDiagnostic( period=diag_period, top=top, w3d=w3d,
            species={ species.name : species for species in listofallspecies }, 
            comm_world=comm_world, lparallel_output=parallel_output )
    installafterstep( diag2.write )

print('\nInitialization complete\n')

# -----------------------------------------------------------------------------
# Simulation loop (Normal users should not modify this part either.)
# -----------------------------------------------------------------------------

# Non-interactive mode
if interactive==0:
    n_stepped=0
    while n_stepped < N_steps:
        step(10)
        n_stepped = n_stepped + 10
    dump()
        
# Interactive mode
elif interactive==1:
    print '<<< To execute n steps, type "step(n)" at the prompt >>>'
